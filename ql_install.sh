set -x \
&& echo -e "\n\
export QL_DIR=/ql\n\
export LANG=zh_CN.UTF-8\n\
export QL_BRANCH=develop\n\
export SHELL=/bin/bash\n\
export TERMUX_APK_RELEASE=F-DROID\n\
export QL_URL=https://github.com/whyour/qinglong.git\n\
export PNPM_HOME=~/.local/share/pnpm\n\
export PATH=$PATH:~/.local/share/pnpm:~/.local/share/pnpm/global/5/node_modules\n" \ > /etc/profile.d/ql_env.sh \
&& source /etc/profile \
&& echo -e "nameserver 119.29.29.29\n\
nameserver 8.8.8.8" > /etc/resolv.conf \
&& sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories \
&& apk update -f \
&& apk upgrade \
&& apk --no-cache add -f bash make coreutils moreutils git curl wget tzdata perl openssl nginx nodejs jq openssh npm python3 py3-pip \
&& rm -rf /var/cache/apk/* \
&& apk update \
&& ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
&& echo "Asia/Shanghai" > /etc/timezone \
&& git config --global user.email "qinglong@@users.noreply.github.com" \
&& git config --global user.name "qinglong" \
&& git config --global http.postBuffer 524288000 \
&& npm config set registry https://registry.npmmirror.com \
&& npm install -g pnpm \
&& pnpm add -g pm2 ts-node typescript tslib \
&& curl -sLo package.json https://ghproxy.com/https://raw.githubusercontent.com/whyour/qinglong/$QL_BRANCH/package.json \
&& pnpm install --prod \
&& read -p "开始拉取青龙项目，自行确认github能否正常访问，建议开启魔法梯，当然你能保证github能正常访问无需魔法梯，输入yes拉取青龙，no退出脚本(yes|no):" Choose
case $Choose in
yes)
 mkdir -p $QL_DIR \
 && cd $QL_DIR \
 && git clone -b $QL_BRANCH $QL_URL $QL_DIR \
 && cp -f .env.example .env \
 && cp -rf /root/node_modules $QL_DIR \
 && cp -f /root/package.json $QL_DIR \
 && cp -f /root/pnpm-lock.yaml $QL_DIR \
 && chmod 777 $QL_DIR/shell/*.sh \
 && chmod 777 $QL_DIR/docker/*.sh \
 && rm -rf /root/.pnpm-store \
 && rm -rf /root/.local/share/pnpm/store \
 && rm -rf /root/.cache /root/.npm \
 && mkdir -p $QL_DIR/static \
 && git clone -b $QL_BRANCH https://github.com/whyour/qinglong-static.git /static \
 && cp -rf /static/* $QL_DIR/static \
 && rm -rf /static /root/node_modules /root/package.json /root/pnpm-lock.yaml \
 && sed -i 's/var\/log\/nginx\/error.log/dev\/null/g' $QL_DIR/docker/nginx.conf \
 && sed -i 's/crond -f/crond/' $QL_DIR/docker/docker-entrypoint.sh \
 && rm -rf /root/.config /root/.pm2 /root/.ssh /root/.ash_history /root/.npmrc \
 && echo "青龙面板将在15秒后启动，由于首次启动会再次安装npm依赖，因为是国内源，魔法梯会导致面板启动缓慢，现在你可以关闭魔法梯了" \
 && sleep 15 \
 && ln -sf /proc/mounts /etc/mtab \
 && ln -sf /ql/docker/docker-entrypoint.sh /usr/bin/qinglong \
 && qinglong
 ;;
no|*)
 exit 0
 ;;
esac

