# 该脚本将在卸载期间执行，您可以编写自定义卸载规则
export rootfs=$MODPATH/gzname

unmount()
{
  pids=$(lsof | grep "$rootfs" | awk '{print $1}' | uniq)
  pkill -9 ${pids}

    for umount_dir in $(cat /proc/mounts | awk '{print $2}' | grep "^$rootfs" | sort -r)
    do
        umount ${umount_dir}
        wait
    done
}

unmount
rm -rf $rootfs
