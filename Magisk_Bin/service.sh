# 开机之后执行
#!/system/bin/sh
# 不要假设您的模块将位于何处。
# 如果您需要知道此防跳和模块的放置位置，请使用$MODDIR
# 这将确保您的模块仍能正常工作
# 即使Magisk将来更改其挂载点
while [ "$(getprop sys.boot_completed)" != "1" ]; do
  sleep 3
done

export rootfs=$MODPATH/gzname

mount_status()
{
  mount_var="$1"
  if grep -q " ${mount_var%/} " /proc/mounts; then
    return 0
  else
    return 1
  fi
}

mountdir()
{
  for ex_var in /dev/fd /dev/stdin /dev/stout /dev/sterr /dev/tty0 /dev/net/tun /proc/sys/fs/binfmt_misc;
  do
    if [ ! -e ${ex_var} ] && [ ! -h ${ex_var} ]; then
      case ${ex_var} in
      /dev/fd)
        ln -s /proc/self/fd ${ex_var}
      ;;
      /dev/stdin)
        ln -s /proc/self/fd/0 ${ex_var}
      ;;
      /dev/stdout)
        ln -s /proc/self/fd/1 ${ex_var}
      ;;
      /dev/stderr)
        ln -s /proc/self/fd/2 ${ex_var}
      ;;
      /dev/tty0)
        ln -s /dev/null ${ex_var}
      ;;
      /dev/net/tun)
        [ -d "/dev/net" ] || mkdir -p /dev/net
        mknod /dev/net/tun c 10 200
      ;;
      /proc/sys/fs/binfmt_misc)
      mount -t binfmt_misc binfmt_misc "/proc/sys/fs/binfmt_misc"
      ;;
      esac
    fi
  done

  for mount_dir in / /dev /proc /sys /dev/shm /dev/pts /tmp;
  do
    if ! mount_status "$rootfs${mount_dir}"; then
      case ${mount_dir} in
        /)
          mount --bind $rootfs $rootfs/
          mount -o remount,exec,suid,dev "$rootfs"
        ;;
        /dev)
          [ -d "$rootfs/dev" ] || mkdir -p "$rootfs/dev"
          mount --bind /dev "$rootfs/dev"
        ;;
        /proc)
          [ -d "$rootfs/proc" ] || mkdir -p "$rootfs/proc"
          mount -t proc proc "$rootfs/proc"
        ;;
        /sys)
          [ -d "$rootfs/sys" ] || mkdir -p "$rootfs/sys"
          mount -t sysfs sys "$rootfs/sys"
        ;;
        /dev/shm)
          [ -d "/dev/shm" ] || mkdir -p /dev/shm
          mount -o rw,nosuid,nodev,mode=1777 -t tmpfs tmpfs /dev/shm
          [ -d "$rootfs/dev/shm" ] || mkdir -p $rootfs/dev/shm
          mount --bind /dev/shm "$rootfs/dev/shm"
        ;;
        /dev/pts)
          [ -d "/dev/pts" ] || mkdir -p /dev/pts
          mount -o rw,nosuid,noexec,gid=5,mode=620,ptmxmode=000 -t devpts devpts /dev/pts
          [ -d "$rootfs/dev/pts" ] || mkdir -p $rootfs/dev/pts
          mount --bind /dev/pts "$rootfs/dev/pts"
        ;;
        /tmp)
          [ -d "$rootfs/tmp" ] || mkdir -p "$rootfs/tmp"
          mount -t tmpfs tmpfs "$rootfs/tmp"
        ;;
      esac
    fi
  done
}

mountdir
#cp /system/etc/hosts $rootfs/etc/hosts
#echo "nameserver 119.29.29.29" > $rootfs/etc/resolv.conf
#echo "nameserver 180.76.76.76" >> $rootfs/etc/resolv.conf
#grep -rl '$QL_DIR' $rootfs/ql/shell | xargs sed -i "s/\$QL_DIR/\/ql/g"
sed -i 's/var\/log\/nginx\/error.log/dev\/null/g' $rootfs/ql/docker/nginx.conf
sed -i 's/crond -f/crond/' $rootfs/ql/docker/docker-entrypoint.sh
chroot $rootfs /usr/bin/env -i HOME=/root PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/.local/share/pnpm:/root/.local/share/pnpm/global/5/node_modules:/root/.local/share/pnpm TERM=linux SHELL=/bin/bash LANG=zh_CN.UTF-8
 /bin/ash -c "/usr/sbin/sshd -f /etc/ssh/sshd_config; source /root/.bashrc && cd /ql && /ql/docker/docker-entrypoint.sh"